package com.ezm.qrcodescaner;

import android.graphics.PointF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView.OnQRCodeReadListener;

public class MainActivity extends AppCompatActivity  implements OnQRCodeReadListener {

	private QRCodeReaderView qrCodeReaderView;
	private static final String MOVIE_DETAIL = "movie_detail";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		qrCodeReaderView = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
		qrCodeReaderView.setOnQRCodeReadListener(this);

		// Use this function to enable/disable decoding
		qrCodeReaderView.setQRDecodingEnabled(true);

		// Use this function to change the autofocus interval (default is 5 secs)
		qrCodeReaderView.setAutofocusInterval(2000L);

		// Use this function to enable/disable Torch
		qrCodeReaderView.setTorchEnabled(true);

		// Use this function to set back camera preview
		qrCodeReaderView.setBackCamera();
	}

	@Override
	public void onQRCodeRead(String text, PointF[] points) {
		if (text.contains(MOVIE_DETAIL)){
			//Take user to movie detail
			String movieID = null;
			if (text.contains("/")){
				String[] splits = text.split("/");
				if (splits.length > 0) {
					movieID = text.split("/")[1];
				}
			}

			if (movieID != null){
				//TODO : Take user to movie detail page with movieID
				Toast.makeText(this, movieID, Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		qrCodeReaderView.startCamera();
	}

	@Override
	protected void onPause() {
		super.onPause();
		qrCodeReaderView.stopCamera();
	}
}
